var dia = "2011-02-02"; //arbitario!
var json_url = "data/votacoes/" + dia + ".json"; //arbitario!
var json_vereadores = "data/vereadores.json";
var json_calendar = "data/calendar.json";

function carregaParlamentares() {

    $('#parlamentares_container').text('');   
    $.getJSON(json_vereadores, function(data) {
    $.each(data, function(key, vereador) {
        vereador['partido-lower'] = vereador['partido'].toLowerCase();
        var vereador_html = ich.vereador_img(vereador);
        $('#parlamentares_container').append(vereador_html);
        });
    });
}

function carregaVotacoes(dia) {
    votacoes_json_url = "data/votacoes/" + dia + ".json";
    $.getJSON(votacoes_json_url, function(data) {
        $.each(data, function(key, votacao_data) {      
			//tudo maiuscula
			votacao_data.materia = votacao_data.materia.toUpperCase();
			votacao_data.tipo = votacao_data.tipo.toUpperCase();
			votacao_data.resultado = votacao_data.resultado.toUpperCase();
            
            //conserta pendente
            if (votacao_data.resultado.indexOf("PENDENTE") != -1) {
                votacao_data.resultado = "PENDENTE";
            }
            votacao_data.resultado_class = votacao_data.resultado.toLowerCase();
            
            //separa estilos para votacoes com ementa e sem
            if (votacao_data.ementa != '') {
                votacao_html = ich.votacao_pl(votacao_data);
            }
            else {
                votacao_html = ich.votacao_other(votacao_data);
            }
            $('#votacoes_container').append(votacao_html);
            });
    });
}

function carregaPresencas(dia) {    
    presencas_json_url = "data/presencas/" + dia + ".json";
    $.getJSON(presencas_json_url, function(data) {
        $.each(data['vereadores'], function(key, vereador) {
            var vereador_id = "#vereador-"+vereador.id;
            if (vereador.presenca) {
                $(vereador_id).addClass("presente");
            }
            else {
                $(vereador_id).addClass("ausente");
            }
        });
    });
     setTimeout("$('.ausente').detach().appendTo('#parlamentares_container');",50); //very hackish!
     
}

function sortPartidos(vereadores) {
    vereadores.sortElements(function(a, b){
        //return $("[partido='"+$(b).attr("partido")+"']").length > $("[partido='"+$(a).attr("partido")+"']").length ? 1 : -1;
        return $(b).attr("partido") > $(a).attr("partido") ? 1 : -1;
    });
}

function mudaData(direcao) {
   $.getJSON(json_calendar, function(calendar) {
       data_atual = calendar.indexOf(dia);
            if (direcao == "proximo") {
                if (data_atual < calendar.length-1) {
                    window.location = "./?data="+calendar[data_atual+1]+"/";
                }
                else {
                    window.location = "./?data="+calendar[0]+"/";
                }
            }
            else if (direcao == "anterior") {
                if (data_atual > 0) {
                    window.location = "./?data="+calendar[data_atual-1]+"/";
                }
                else if (data_atual == 0) {
                    window.location = "./?data="+calendar[calendar.length-1]+"/";
                }
            }
       });
   }

$(document).ready(function() {        
        //carrega data da url
        if (gup("data")) {
            dia = gup("data");
        }
        
        
        //define o titulo da página
        d = dia.split("-");
        var camara = {
            'data' : d[2]+'-'+d[1]+'-'+d[0]
        }
        
        $('#header').replaceWith(ich.cabec(camara));
        
        //itera pelas votacoes do dia
        carregaVotacoes(dia);
        carregaParlamentares();
        setTimeout("carregaPresencas(dia);",50); //very hackish!
        
    });
