# -*- coding: utf-8 -*-
import urllib
from BeautifulSoup import BeautifulStoneSoup
import datetime
import json
from operator import itemgetter
import difflib

def scrapeVotacoes(xml, total_presenca):
    soup = BeautifulStoneSoup(xml)
    votacoes = []
    for v in soup.cmsp.findAll("votacao"):
        votacao = {}
        votacao['id'] = v.get("votacaoid")
        votacao['materia'] = v.get("materia")
        votacao['tipo'] = v.get("tipovotacao")
        votacao['resultado'] = v.get("resultado")
        votacao["data"] = datetime.datetime.strptime(v.get("datadasessao"), "%d/%m/%Y").isoformat()
        votacao["ementa"] = v.get("ementa")
        votacao["notas"] = v.get("notasrodape")
        
        #Votos
        votacao["votos"] = {} #preencher
        for p in v.findAll("vereador"):
            votacao["votos"][p.get("idparlamentar")] = {
                'nome' : p.get("nomeparlamentar"),
                'voto' : p.get("voto"),
                'partido': p.get("partido")
                }
        
        
        #Votos consolidados
        if v.get("tipovotacao") == "Nominal":
            votacao["presentes"] = int(v.get("npresentes"))
            votacao["votos_consolidado"] = {
            "sim" : int(v.get("nsim")),
            "nao" : int(v.get("nnao")),
            "branco" : int(v.get("nbranco")),
            "abstencao" : int(v.get("nabstencao"))
            }
        elif v.get("tipovotacao") == u"Simbólica":
            votacao["presentes"] = total_presenca #Definido arbitrariamente como o total de presencas
            nnao = 0
            for voto in v.findAll("vereador"):
                if voto.get("voto") == u"Contrário":
                    nnao += 1
            votacao["votos_consolidado"] = {
            "nao" : nnao,
            "sim" : votacao["presentes"] - nnao,
            "abstencao" : 0,
            "branco" : 0
            }
        votacoes.append(votacao)
    return votacoes
    
def separatebyDate(votacoes):
    votacoes_por_dia = {}
    for votacao in votacoes:
        data_key = votacao['data']
        if votacoes_por_dia.has_key(data_key):
            votacoes_por_dia[data_key].append(votacao)
        else:
            votacoes_por_dia[data_key] = [votacao]
    return votacoes_por_dia
    
def generateCalendar(votacoes): #coletar numero de votos
    calendar = []
    for votacao in votacoes:
        data_key = votacao['data'].strip('T00:00:00')
        if data_key in calendar:
            pass
        else:
            calendar.append(data_key)
    jason = open('data/calendar.json', 'w')
    jason.write(json.dumps(calendar))
    jason.close()
    return calendar


def compilaVotacoes(votacoes_url):
    xml = urllib.urlopen(votacoes_url)
    votacoes = scrapeVotacoes(xml, 0)
    votacoes_por_dia = separatebyDate(votacoes)
    for dia in votacoes_por_dia:
        jason = open("data/votacoes/"+str(dia[:10])+'.json', 'w')
        jason.write(json.dumps(votacoes_por_dia[dia]))
        jason.close()


###### Presenças ######

def scrapePresencas(xml):
    soup = BeautifulStoneSoup(xml)
    presencas = {}
    presencas['data'] = datetime.datetime.strptime(soup.cmsp.presencas.get("data"), "%d/%m/%Y").isoformat()
    presencas['vereadores'] = []
    for v in soup.cmsp.findAll("vereador"):
        vereador = {}
        vereador['nome'] = v.get("nomeparlamentar")
        vereador['id'] = int(v.get("idparlamentar"))
        vereador['partido'] = v.get("partido")
        vereador['presenca_txt'] = v.get("presente")
        if v.get("idevento") == "6":
            vereador['presenca'] = True
        else:
            vereador['presenca'] = False
        #vereador['idevento'] = int(v.get("idevento"))
        #vereador['acaoevento'] = v.get("acaoevento")
        if v.get("horaevento"):
            vereador['horaevento'] = datetime.datetime.strptime(v.get("horaevento"), "%d/%m/%Y %H:%M:%S").isoformat()
        else:
            vereador['horaevento'] = ''
        presencas['vereadores'].append(vereador)
    
    presencas['consolidado'] = {}
    presencas['consolidado']['presentes'] = 0
    presencas['consolidado']['ausentes'] = 0
    
    for presenca in presencas['vereadores']:
        if presenca['presenca']:
            presencas['consolidado']['presentes'] += 1
        else:
            presencas['consolidado']['ausentes'] += 1
    return presencas

def compilaPresencas(presencas_url):
    xml = urllib.urlopen(presencas_url)
    presencas = scrapePresencas(xml)
    jason = open("data/presencas/"+str(presencas['data'][:10])+'.json', 'w')
    jason.write(json.dumps(presencas))
    jason.close()


#tempfix
def rockandroll(votacoes_url):
    xml = urllib.urlopen(votacoes_url)
    votacoes = scrapeVotacoes(xml, 0)
    votacoes_por_dia = separatebyDate(votacoes)
    base_url = "http://www2.camara.sp.gov.br/SIP/BaixarXML.aspx?arquivo="
    for dia in votacoes_por_dia:
        print 'Baixando dia ' + str(dia)
        xml_filename = "Presencas_" + '_'.join([dia[:4],dia[5:7],dia[8:10]])+ "_[0].xml"
        url = base_url + xml_filename
        print url
        raw_xml = urllib.urlopen(url)
        raw_file = open("data/raw/"+xml_filename, 'w')
        raw_file.write(raw_xml.read())
        raw_file.close()
        
        print 'Gerando JSON'
        compilaPresencas("data/raw/"+xml_filename)


def vereadores_e_fotos():
    idlist = open("data/presencas/2011-03-23.json")
    idlist = json.load(idlist)
    fotoslist = urllib.urlopen("https://api.scraperwiki.com/api/1.0/datastore/sqlite?format=json&name=vereadoressp_1&query=select+*+from+%60swdata%60&apikey=")
    fotoslist = json.load(fotoslist)
    for i, vereador in enumerate(fotoslist):
        possibilities = []
        for v in idlist['vereadores']:
            match = {}
            match['id'] = v['id']
            match['partido'] = v['partido']
            match['chance'] = difflib.SequenceMatcher(None, v['nome'].strip(), vereador['nome'].strip()).ratio()
            print v['nome'] + ' ' + vereador['nome'] + ' ' + str(match['chance'])
            possibilities.append(match)
        fotoslist[i]['id'] = sorted(possibilities, key=itemgetter("chance"), reverse=True)[0]['id']
        fotoslist[i]['partido'] = sorted(possibilities, key=itemgetter("chance"), reverse=True)[0]['partido']

    jason = open('data/vereadores.json', 'w')
    jason.write(json.dumps(fotoslist))
    jason.close()


votacoes_url = "data/raw/Votacoes_2011.xml"
xml = urllib.urlopen(votacoes_url)
votacoes = scrapeVotacoes(xml, 0)
calendar = generateCalendar(votacoes)
print calendar
